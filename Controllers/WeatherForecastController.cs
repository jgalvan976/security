﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace WebApplication1.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }


        [HttpPost]
        //[Route("weather/create")]
        //[CustomModelValidate]
        public IActionResult CreateWeather([FromBody]WeatherForecast model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok();
        }
    }

    public class CustomModelValidate : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                 var errors = context.ModelState.Values.SelectMany(x => x.Errors.Select(p => new ErrorModel()
                    {
                        ErrorCode = p.ErrorMessage.Contains("could not be converted")? (int)HandleErrorApi.BadRequestValueNotConverted: 
                                                                                        (int)HandleErrorApi.BadRequestPropertyIsRequired,//((int)HttpStatusCode.BadRequest).ToString(CultureInfo.CurrentCulture),
                        //ErrorMessage = p.ErrorMessage,
                        ServerErrorMessage = p.Exception?.InnerException.ToString(),
                        
                    })).ToList();
                    var result = new BaseResponse
                    {
                        Errors = errors,
                        ResponseCode = (int)HttpStatusCode.BadRequest,
                        //ResponseMessage = ResponseMessageConstants.VALIDATIONFAIL,

                    };
                context.Result = new BadRequestObjectResult(result);
            }
        }
    }

    public enum HandleErrorApi
    {
        BadRequestValueNotConverted = 4001,
        BadRequestPropertyIsRequired = 4002

    }
}
