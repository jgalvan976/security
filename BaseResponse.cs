﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1
{
    public class BaseResponse
    {
        public List<ErrorModel> Errors { get; set; }
        public int ResponseCode { get; set; }

    }
}
