using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Linq;
using System.Net;

namespace WebApplication1
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            Enviroment = env;
        }

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Enviroment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            ///Filter
            services.AddMvc().ConfigureApiBehaviorOptions(options =>
            {
                options.InvalidModelStateResponseFactory = (context) =>
                {
                    var errors = context.ModelState.Values.SelectMany(x => x.Errors.Select(p => new ErrorModel()
                    {
                        ErrorCode = ((int)HttpStatusCode.BadRequest),
                        ErrorMessage = Enviroment.IsDevelopment() ? p.ErrorMessage : Configuration.GetValue<string>("Errors:ErrorMessage"),
                        ServerErrorMessage = Enviroment.IsDevelopment() ? p.Exception?.InnerException.ToString() : Configuration.GetValue<string>("Errors:ServerErrorMessage"),

                    })).ToList();
                    var result = new BaseResponse
                    {
                        ResponseCode = (int)HttpStatusCode.BadRequest,
                        Errors = errors
                  };
                    return new BadRequestObjectResult(result);
                };
            });


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            app.UseHttpsRedirection();
            
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            app.UseDeveloperExceptionPage();


        }
    }
}
